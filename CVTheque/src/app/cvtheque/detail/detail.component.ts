import { Component, OnInit, Input } from '@angular/core';
import { Personne } from 'src/app/Model/Personne';
import { EmbaucheService } from '../embauche.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  @Input() personne : Personne;

  constructor(
    private embaucheService : EmbaucheService
  ) { }

  ngOnInit(): void {
  }

  embaucher(personne : Personne) : void {
    this.embaucheService.embaucher(personne);
  }
}
