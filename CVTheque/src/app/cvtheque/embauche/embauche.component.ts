import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/Model/Personne';
import { EmbaucheService } from '../embauche.service';

@Component({
  selector: 'app-embauche',
  templateUrl: './embauche.component.html',
  styleUrls: ['./embauche.component.scss']
})
export class EmbaucheComponent implements OnInit {

  public personnes : Personne[];

  constructor(
    private embaucheService : EmbaucheService
  ) { }

  ngOnInit(): void {
    this.personnes = this.embaucheService.getEmchauches();
  }

  debaucher(personne : Personne) : void {
    this.embaucheService.debaucher(personne);
  }

}
