import { Injectable } from '@angular/core';
import { Personne } from '../Model/Personne';

@Injectable({
  providedIn: 'root'
})
export class CvService {

  private personnes : Personne[];

  constructor() { 
    this.personnes = [
      new Personne(1, 'Fanny', 'Rouyer', 24, 'Développeur web', 'UI/UX design, Suite Adobe, HTML5, SCSS, PHP, Angular, Laravel et Ionic', 'fanny.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.'),
      new Personne(2, 'Oria', 'Rouyer', 2, 'Chieuse professionnelle', "J'embête ma maman toute la journée ! Je la déconcentre, je lui demande des calins AHAHAH.", 'oria.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
      new Personne(3, 'Truc', 'Muche', 30, 'Blabla', "Blabla blabla blabla", '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.'),
    ];
  }

  getPersonnes() : Personne[] {
    return this.personnes;
  }
}
