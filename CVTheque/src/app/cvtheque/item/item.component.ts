import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Personne } from 'src/app/Model/Personne';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() personnes : Personne[];
  @Output() onePersonneSelected = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  sendOnePersonne(personne : any) : void {
    // console.log('Dans le component item :', personne);
    // Permet d'envoyer les données de la personne sur laquelle on a cliqué vers un autre component (ici, liste)
    this.onePersonneSelected.emit(
      personne
    );
  }

}
