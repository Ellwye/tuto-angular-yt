import { Component, OnInit } from '@angular/core';
import { Personne } from '../../Model/Personne';
import { PremierService } from 'src/app/premier.service';
import { CvService } from '../cv.service';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.scss']
})
export class CvComponent implements OnInit {

  public personnes : Personne[];
  public personneSelected : Personne;

  constructor(
    private premierService : PremierService,
    private cvService : CvService
  ) { }

  ngOnInit(): void {
    this.personnes = this.cvService.getPersonnes();

    this.premierService.addData('data from cv.component.ts');
    this.premierService.logged(this.personnes);
  }

  personneSelect(personne : any) {
    this.personneSelected = personne;
  }
}
