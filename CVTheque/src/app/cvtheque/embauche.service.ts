import { Injectable } from '@angular/core';
import { Personne } from '../Model/Personne';

@Injectable({
  providedIn: 'root'
})
export class EmbaucheService {

  private personnes : Personne[];

  constructor() { 
    this.personnes = [];
  }

  getEmchauches() : Personne[] {
    return this.personnes;
  }

  embaucher(personne : Personne) : void {
    const index = this.personnes.indexOf(personne);
    // S'il n'y a pas d'index trouvé, c'est que je n'ai pas déjà embauchée cette personne, je peux alors l'embaucher.
    if (index < 0) {
      this.personnes.push(personne);
    } else {
      alert(`${personne.first_name} ${personne.last_name} est déjà embauchée.`);
    }
  }

  debaucher(personne : Personne) : void {
    const index = this.personnes.indexOf(personne);
    if (index >= 0) {
      this.personnes.splice(index, 1);
    } else {
      alert(`${personne.first_name} ${personne.last_name} n'est pas embauchée, vous ne pouvez donc pas le/la débaucher.`);
    }
  }
}
