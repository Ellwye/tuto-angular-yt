import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Personne } from 'src/app/Model/Personne';

@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.scss']
})
export class ListeComponent implements OnInit {

  @Input() personnes : Personne[]; // Pour récupérer les données du component cv
  @Output() personneSelectionnee = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  personneSelected(personne : any) : void {
    console.log('Dans le component liste :', personne);
    this.personneSelectionnee.emit(
      personne
    );
  }

}
