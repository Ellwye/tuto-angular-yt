import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CvComponent } from './cvtheque/cv/cv.component';
import { ItemComponent } from './cvtheque/item/item.component';
import { DetailComponent } from './cvtheque/detail/detail.component';
import { ListeComponent } from './cvtheque/liste/liste.component';
import { StyleComponent } from './directive/style/style.component';
import { ClassComponent } from './directive/class/class.component';
import { HighlightDirective } from './directive/highlight.directive';
import { RainbowDirective } from './directive/rainbow.directive';
import { DefaultImagePipe } from './cvtheque/default-image.pipe';
import { EmbaucheComponent } from './cvtheque/embauche/embauche.component';

@NgModule({
  declarations: [
    AppComponent,
    CvComponent,
    ItemComponent,
    DetailComponent,
    ListeComponent,
    StyleComponent,
    ClassComponent,
    HighlightDirective,
    RainbowDirective,
    DefaultImagePipe,
    EmbaucheComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
