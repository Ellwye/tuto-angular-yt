import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CvComponent } from './cvtheque/cv/cv.component';
import { StyleComponent } from './directive/style/style.component';
import { ClassComponent } from './directive/class/class.component';


const routes: Routes = [
  { path: '',   redirectTo: 'cv', pathMatch: 'full' },
  { path: 'cv', component: CvComponent },
  { path: 'style', component: StyleComponent },
  { path: 'class', component: ClassComponent }
  // { path: '**', component: PageNotFoundComponent },  // 404 page redirection
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
