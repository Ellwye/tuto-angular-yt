import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PremierService {

  public data : string[] = [
    'data1',
    'data2',
    'data3',
    'data4',
    'data5',
  ];

  constructor() { }

  logged(data : any) : void {
    console.log(this.data);
    console.log(data);
    // return data;
  }

  addData(data : any) : void {
    this.data.push(data);
  }
}
