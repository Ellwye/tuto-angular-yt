export class Personne {
  id : number;
  first_name : string;
  last_name : string;
  age : number;
  job : string;
  job_description : string;
  path : string;
  presentation : string;

  constructor(id : number, first_name : string, last_name : string, age : number, job : string, job_description : string, path : string, presentation : string) {
    this.id = id;
    this.first_name = first_name;
    this.last_name = last_name;
    this.age = age;
    this.job = job;
    this.job_description = job_description;
    this.path = path;
    this.presentation = presentation;
  }
}
