import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  // À chaque changement du "bg" le "background-color" de la propriété style va changer
  @HostBinding('style.background-color') bg : string = '#f59f59';
  @HostBinding('style.border-radius') br : string = '0';
  @HostBinding('style.transition') transition : string = 'ease 0.3s';

  constructor() { }

  // À chaque entrée de la souris sur l'élement, la couleur associée à la propriété "bg" va changer en jaune
  @HostListener('click') mouseClick() : void {
    if (this.bg === '#f562f562') {
      this.bg = '#f22f22';
    } else {
      this.bg = '#f562f562';
    }
  }

  @HostListener('mouseenter') mouseEnter() : void {
    this.br = '50px';
  }

  // À chaque sortie de la souris sur l'élément, la couleur associée à la propriété "bg" va changer en rouge
  @HostListener('mouseleave') mouseLeave() : void {
    this.bg = '#f56f56';
    this.br = '0';
  }

}
