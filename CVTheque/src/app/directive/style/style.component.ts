import { Component, OnInit, Input } from '@angular/core';
import { PremierService } from 'src/app/premier.service';

@Component({
  selector: 'app-style',
  templateUrl: './style.component.html',
  styleUrls: ['./style.component.scss'],
  providers: [PremierService] // permet d'avoir une autre instance, pas celle utilisée par tous les components
})
export class StyleComponent implements OnInit {

  public color : string = 'white';
  public bgColor : string = 'lightblue';
  public size : string = '20px';
  // Pour utiliser en passant des variables via le <app-style>
  // @Input() public color : string = 'white';
  // @Input() public bgColor : string = 'lightblue';

  constructor(
    private premierService : PremierService
  ) { }

  ngOnInit(): void {
  }

  changeSize(size : number) : void {
    this.size = size + 'px';
  }

  logginData() : void {
    this.premierService.logged('test style');
  }

}
