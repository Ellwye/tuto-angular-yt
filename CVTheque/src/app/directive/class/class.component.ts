import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.scss']
})
export class ClassComponent implements OnInit {

  public one : boolean = false;
  public two : boolean = true;
  public last : boolean = false;
  public block : boolean = true;

  public show : boolean = true;

  public team : string[] = [
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'height',
    'nine',
    'ten'
  ];

  public numbers : number[] = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10
  ];

  constructor() { }

  ngOnInit(): void {
  }

  changeColor() : void {
    if (this.one === true) {
      this.one = !this.one;
      this.two = !this.two;
      this.last = this.last;
    } 
    else if (this.two === true) {
      this.one = this.one;
      this.two = !this.two;
      this.last = !this.last;
    } 
    else {
      this.one = !this.one;
      this.two = this.two;
      this.last = !this.last;
    }
  }

}
