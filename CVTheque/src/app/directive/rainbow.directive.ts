import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appRainbow]'
})
export class RainbowDirective {

  public colors : string[] = [
    'blue', 'lightblue', 'coral', 'pink', 'purple', 'lightgreen', 'green', 'yellow', 'gold', 'lightgray'
  ]

  @HostBinding('style.border-color') borderColor : string;
  @HostBinding('style.color') color : string;

  constructor() { }

  @HostListener('keypress') changeColor() : void {
    const i = Math.floor(Math.random() * (this.colors.length -1));
    this.borderColor = this.colors[i];
    const j = Math.floor(Math.random() * (this.colors.length -1));
    this.color = this.colors[j];
  }

}
