import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-child-a',
  templateUrl: './child-a.component.html',
  styleUrls: ['./child-a.component.scss']
})
export class ChildAComponent implements OnInit {

  @Input() childProperty : string; // Permet de récupérer des data du component parent (ici color car <app-child-a> est appelé dans le component color)
  @Output() sendRequestToData = new EventEmitter(); // Permet d'envoyer des data au component parent

  constructor() { }

  ngOnInit(): void {
    console.log('Child component, contenu de la variable color du component color père :', this.childProperty);
  }

  // Function permettant d'envoyer des data avec .emit
  sendEvent() : void {
    this.sendRequestToData.emit(
     'Please can i have sun ?'
    )
  }

}
