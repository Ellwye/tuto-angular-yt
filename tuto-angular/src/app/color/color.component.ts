import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.scss']
})
export class ColorComponent implements OnInit {
  
  public color : string = "#f59985";
  public color2 : string;

  constructor() { }

  ngOnInit(): void {
  }

  // Event binding 
  // #inputColor (change)="changeColor(inputColor)"
  // [style.background-color]="color"

  changeColor(input : any) : void {
    console.log(input.value);
    this.color = input.value;
    input.value = ''; // Permet de remettre l'input vide
  }

  // Function permettant de récupérer les data emit
  processReq(data : any) : void {
    alert(data);
  }

}
