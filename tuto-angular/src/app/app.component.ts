import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  // Interpolation {{ }}

  title : string = 'tuto-angular';
  bgColor : string = "#f56f56";
  hidden : boolean = false;
  buttonName : string = 'Hide';

  changeStatus() {
    this.hidden = !this.hidden;
    if (this.hidden === true) {
      this.buttonName = 'Show';
    } else {
      this.buttonName = 'Hide';
    }
  }
}
